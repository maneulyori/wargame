package com.me.wargame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.math.Rectangle;

public class GameContext{
	private Function function;
	private static int Mode;
	private static Texture mapImage;
	private static Texture diceImage;
	private static int dice;
	private static int turn;
	private static Pin pinA;
	private static Pin pinB;
	private static Block[] block;
	private static int BlockWidthSize=6;
	private static int BlockHeightSize=5;
	private static int BlockSize=BlockHeightSize * BlockWidthSize - (BlockHeightSize - 2) * (BlockWidthSize - 2);
	private int width=Gdx.graphics.getWidth() * 120 / 800;
	private int height=Gdx.graphics.getHeight() * 80 / 480;
	private int x=Gdx.graphics.getWidth() * 240 / 800;
	private int y=Gdx.graphics.getHeight() * 420 / 480;
	private int Ax, Ay, Bx, By;
	private int i=0;
	private static WarGame wargame;
	public static final int[] buildingPos = new int[] {1, 4, 6, 7, 10, 13, 15, 16};
	
	public GameContext(WarGame warGame)
	{		
		GameContext.turn = 1;
		GameContext.dice = 0;
		GameContext.wargame = warGame;
		
		GameContext.setDiceImage(new Texture(Gdx.files.internal("data/dice1.png")));
		GameContext.setMapImage(new Texture(Gdx.files.internal("data/map.png")));
		
		pinA = new Pin(new Texture(Gdx.files.internal("data/characterA.png")),
				new Texture(Gdx.files.internal("data/Astate.png")), 1, warGame);
		pinA.setPosition(0);
		pinA.setSoldier(1000);

		pinB = new Pin(new Texture(Gdx.files.internal("data/characterB.png")), 
				new Texture(Gdx.files.internal("data/Bstate.png")), 2, warGame);
		pinB.setPosition(9);
		pinB.setSoldier(1000);

		GameContext.block=new Block[BlockSize];		
		
		for(i=0;i<BlockSize;++i)
		{
			block[i] = new Block();
			
			if(i == 2 || i == 11)	
				block[i].setTexture(new Texture(Gdx.files.internal("data/tunnel.png")));
			else if(i == 3 || i == 12)
				block[i].setTexture(new Texture(Gdx.files.internal("data/treasure.png")));
			else if(i == 5)
				block[i].setTexture(new Texture(Gdx.files.internal("data/AirBomb.png")));
			else if(i == 8 || i == 17)
				block[i].setTexture(new Texture(Gdx.files.internal("data/smog.png")));
			else if(i == 14)
				block[i].setTexture(new Texture(Gdx.files.internal("data/GroundSupport.png")));
			else if(i == 1 || i == 4)
				block[i].setTexture(new Texture(Gdx.files.internal("data/block1.png")));
			else if(i == 6 || i == 7)
				block[i].setTexture(new Texture(Gdx.files.internal("data/block2.png")));
			else if(i == 10 || i == 13)
				block[i].setTexture(new Texture(Gdx.files.internal("data/block3.png")));
			else if(i == 15 || i == 16)
				block[i].setTexture(new Texture(Gdx.files.internal("data/block4.png")));


			block[i].getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);

			if(i >=0 && i < BlockWidthSize){
				x += width / 2;
				y -= height / 2;
				Ax = x + Gdx.graphics.getWidth() * 50 / 800;
				Ay = y + Gdx.graphics.getHeight() * 50 / 480;
				Bx = x + Gdx.graphics.getWidth() * 80 / 800;
				By = y + Gdx.graphics.getHeight() * 30 / 480;
			}
			else if(i >= BlockWidthSize && i < BlockWidthSize + BlockHeightSize -1){
				x -= width / 2;
				y -= height / 2;
				Ax = x + Gdx.graphics.getWidth() * 50 / 800;
				Ay = y + Gdx.graphics.getHeight() * 10 / 480;
				Bx = x + Gdx.graphics.getWidth() * 80 / 800;
				By = y + Gdx.graphics.getHeight() * 30 / 480;
			}
			else if(i >= BlockWidthSize + BlockHeightSize -1 && i < BlockWidthSize * 2 + BlockHeightSize - 2){
				x -= width / 2;
				y += height / 2;
				Ax = x + Gdx.graphics.getWidth() * 20 / 800;
				Ay = y + Gdx.graphics.getHeight() * 30 / 480;
				Bx = x + Gdx.graphics.getWidth() * 50 / 800;
				By = y + Gdx.graphics.getHeight() * 10 / 480;
			}
			else if(i >= BlockWidthSize * 2 + BlockHeightSize - 2 && i < BlockSize){
				x += width / 2;
				y += height / 2;
				Ax = x + Gdx.graphics.getWidth() * 20 / 800;
				Ay = y + Gdx.graphics.getHeight() * 30 / 480;
				Bx = x + Gdx.graphics.getWidth() * 50 / 800;
				By = y + Gdx.graphics.getHeight() * 50 / 480;
			}
			if(i == 0 || i == BlockWidthSize-1 || i == BlockWidthSize + BlockHeightSize - 2 || i == BlockWidthSize * 2 + BlockHeightSize - 3){
				Ax = x + Gdx.graphics.getWidth() * 20 / 800;
				Ay = y + Gdx.graphics.getHeight() * 30 / 480;
				Bx = x + Gdx.graphics.getWidth() * 80 / 800;
				By = y + Gdx.graphics.getHeight() * 30 / 480;
			}
			
			int sizeX = Gdx.graphics.getWidth() * 20 / 800;
			int sizeY = Gdx.graphics.getHeight() * 20 / 480;
			
			System.out.println(Integer.toString(x) + " " + Integer.toString(y) + " " + Integer.toString(width) + " " + Integer.toString(height));
			block[i].setRectangle(new Rectangle(x, y, width, height), new Rectangle(Ax, Ay, sizeX, sizeY), new Rectangle(Bx, By, sizeX, sizeY));
		}
	}
	
	public void setFunction(Function function)
	{
        if (this.function != null) {
            this.function.dispose();
        }

        this.function = function;
        
        Gdx.input.setInputProcessor(function);
	}
	
	public Function getFunction()
	{
		return function;
	}
	
	public static void setMode(int Mode)
	{
		GameContext.Mode = Mode;
	}
	
	public static int getMode()
	{
		return GameContext.Mode;
	}
	
	public static void setMapImage(Texture mapImage)
	{
		GameContext.mapImage = mapImage;
	}
	
	public static Texture getMapImage()
	{
		return GameContext.mapImage;
	}
	
	public static void setDiceImage(Texture diceImage)
	{
		GameContext.diceImage = diceImage;
	}
	
	public static Texture getDiceImage()
	{
		return GameContext.diceImage;
	}

	public static int getBlockSize()
	{
		return GameContext.BlockSize;
	}
	
	public static void setDice(int dice)
	{
		GameContext.dice = dice;
	}
	
	public static int getDice()
	{
		return GameContext.dice;
	}
	
	public static void setTurn(int turn)
	{
		GameContext.turn = turn;
		wargame.broadcast(("TURN_CNAHGE " + Integer.toString(turn)));
		for (StackTraceElement ste : Thread.currentThread().getStackTrace()) {
		    System.out.println(ste);
		}
	}
	
	public static int getTurn()
	{
		return GameContext.turn;
	}
	
	public static Block[] getBlock()
	{
		return GameContext.block;
	}
		
	public static Pin getPinA()
	{
		return GameContext.pinA;
	}
	
	public static Pin getPinB()
	{
		return GameContext.pinB;
	}
	
	public static int convertX(int x)
	{
		return Gdx.graphics.getWidth() * x / 800;
	}

	public static int convertY(int y)
	{
		return Gdx.graphics.getHeight() * y / 480;
	}
}