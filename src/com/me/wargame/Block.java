package com.me.wargame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.graphics.g2d.BitmapFont;

public class Block{
	private int soldier;
	private int trap;
	private Rectangle rect, Arect, Brect;
	private Polygon poly;
	private Texture texture;
	private int team;
    private BitmapFont font;
	Texture blank = new Texture(Gdx.files.internal("data/block.png"));
	Texture Ablock = new Texture(Gdx.files.internal("data/Ablock.png"));
	Texture Bblock = new Texture(Gdx.files.internal("data/Bblock.png"));
	
	public void setRectangle(Rectangle rect, Rectangle Arect, Rectangle Brect)
	{
		this.rect = rect;
		this.Arect = Arect;
		this.Brect = Brect;

		float verticies[]={rect.x, rect.y+40, rect.x+60, rect.y+80, rect.x+120, rect.y+40, rect.x+60, rect.y};
		poly = new Polygon(verticies);

		font = new BitmapFont();
	}
	
	public Rectangle getRectangle()
	{
		return rect;
	}
	
	public Rectangle getArectangle(){
		return Arect;
	}
	public Rectangle getBrectangle(){
		return Brect;
	}
	public Polygon getPolygon(){
		return poly;
	}
	
	void station(int team, int soldier){
		this.team = team;
		this.soldier+=soldier;
	}
	
	void setSoldier(int soldier)
	{
		this.soldier=soldier;
	}
	int getSoldier()
	{
		return soldier;
	}
	
	Block()
	{
		this.texture=blank;
		this.soldier = 0;
		this.team = 0;
	}
	
	void setTexture(Texture texture)
	{
		this.texture=texture;
	}
	
	Texture getTexture()
	{
		return texture;
	}
	int getTeam(){
		return team;
	}
	boolean isStation() {
		if(this.team == 0){
			return false;
		}
		else{
			return true;
		}
	}
	void setTrap(int trap)
	{
		this.trap = trap;
	}
	int getTrap()
	{
		return trap;
	}
	void battle(int soldier){
		int soldierEq = this.soldier*2;
		soldierEq-=soldier;
		if(soldierEq<0){
			this.team=0;
			this.soldier=0;
		}
	}
	public void Draw(SpriteBatch spriteBatch)
	{
	    font.setColor(Color.BLACK);
	    spriteBatch.draw(texture, rect.x, rect.y, rect.width, rect.height);
		if(this.soldier>0){
			font.draw(spriteBatch, Integer.toString(this.soldier), rect.x + 10, rect.y + 50);
		}
	}
}