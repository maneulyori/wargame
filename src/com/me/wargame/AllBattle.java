package com.me.wargame;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class AllBattle implements Function{
	
	private Pin pin;
	
	AllBattle(Pin pin)
	{
		this.pin = pin;
	}

	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void render(SpriteBatch batch) {
		// TODO Auto-generated method stub
		System.out.println("TotalWar");
		for(int i=0;i<GameContext.getBlockSize();++i)
			GameContext.getBlock()[i].Draw(batch);

		if(GameContext.getPinA().getSoldier() >= (GameContext.getPinB().getSoldier() * 2)){
			GameContext.getPinA().loseSoldier(GameContext.getPinB().getSoldier());
			GameContext.getPinA().losetotalSoldier(GameContext.getPinB().getSoldier());
			GameContext.getPinB().loseSoldier(GameContext.getPinA().getSoldier());
			GameContext.getPinB().losetotalSoldier(GameContext.getPinA().getSoldier());
		}
		else if(GameContext.getPinB().getSoldier() >= (GameContext.getPinA().getSoldier()*2)){
			GameContext.getPinB().loseSoldier(GameContext.getPinA().getSoldier());
			GameContext.getPinB().losetotalSoldier(GameContext.getPinA().getSoldier());
			GameContext.getPinA().loseSoldier(GameContext.getPinB().getSoldier());
			GameContext.getPinA().losetotalSoldier(GameContext.getPinB().getSoldier());
		}
		
		else if(GameContext.getPinA().getSoldier() > GameContext.getPinB().getSoldier()){
			GameContext.getPinA().loseSoldier(GameContext.getPinB().getSoldier());
			GameContext.getPinA().losetotalSoldier(GameContext.getPinB().getSoldier());
			GameContext.getPinB().loseSoldier(GameContext.getPinA().getSoldier());
			GameContext.getPinB().losetotalSoldier(GameContext.getPinA().getSoldier());
			
		}
		
		else if(GameContext.getPinB().getSoldier() > GameContext.getPinA().getSoldier()){
			GameContext.getPinB().loseSoldier(GameContext.getPinA().getSoldier());
			GameContext.getPinB().losetotalSoldier(GameContext.getPinA().getSoldier());
			GameContext.getPinA().loseSoldier(GameContext.getPinB().getSoldier());
			GameContext.getPinA().losetotalSoldier(GameContext.getPinB().getSoldier());
		}
		
		pin.changeDirection();
		
		if(GameContext.getPinA().getDirection() == GameContext.getPinB().getDirection()){
			if(pin.getTeam() == 1){
				GameContext.getPinB().changeDirection();
			}
			else if(pin.getTeam() == 2){
				GameContext.getPinA().changeDirection();
			}
		}
		
		GameContext.getPinA().Draw(batch);
		GameContext.getPinB().Draw(batch);
		
		WarGame.context.setFunction(new MainMap());		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
	}
}