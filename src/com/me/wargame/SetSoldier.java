package com.me.wargame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class SetSoldier implements Function{

	private int temp;
    private BitmapFont font;
	private Vector2 inputPos;
	private Rectangle arrow1, arrow2, ok;
	private Texture UP, DOWN, OK;
	private Pin pin;
	
	public SetSoldier(Pin pin)
	{
		this.pin = pin;
	    font = new BitmapFont();
	    font.setColor(Color.BLACK);
		inputPos = new Vector2();
		arrow1 = new Rectangle(GameContext.convertX(360), GameContext.convertY(245), GameContext.convertX(50), GameContext.convertY(25));
		arrow2 = new Rectangle(GameContext.convertX(360), GameContext.convertY(220), GameContext.convertX(50), GameContext.convertY(25));
		ok = new Rectangle(GameContext.convertX(410), GameContext.convertY(220), GameContext.convertX(50), GameContext.convertY(50));
		UP = new Texture(Gdx.files.internal("data/arrowUP.png"));
		DOWN = new Texture(Gdx.files.internal("data/arrowDOWN.png"));
		OK = new Texture(Gdx.files.internal("data/OK.png"));
		
	}
	
	void setTemp(int temp)
	{
		this.temp = 0;
	}
	
	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void render(SpriteBatch batch) {
		// TODO Auto-generated method stub
		batch.draw(GameContext.getMapImage(), 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		
		for(int i=0;i<GameContext.getBlockSize();++i)
			GameContext.getBlock()[i].Draw(batch);

		batch.draw(UP, arrow1.x, arrow1.y, GameContext.convertX(50), GameContext.convertY(25));
		batch.draw(DOWN, arrow2.x, arrow2.y, GameContext.convertX(50), GameContext.convertY(25));
		batch.draw(OK, ok.x, ok.y);
		font.draw(batch, Integer.toString(this.temp), GameContext.convertX(310), GameContext.convertY(250));
		
		if(Gdx.input.justTouched()){
			inputPos.x = Gdx.input.getX();
			inputPos.y = Gdx.graphics.getHeight() - Gdx.input.getY();
			
			if(arrow1.contains(inputPos.x, inputPos.y) && pin.getSoldier() > this.temp && this.temp <= 150){
				this.temp+=50;
			}
			else if(temp>0 && arrow2.contains(inputPos.x, inputPos.y)){
				this.temp-=50;
			}
			else if(ok.contains(inputPos.x, inputPos.y)){
				if(this.temp>0){
					GameContext.getBlock()[pin.getPosition()].station(pin.getTeam(), this.temp);
					if(GameContext.getBlock()[pin.getPosition()].getTeam() == 1){
						if(pin.getPosition() == 1 || pin.getPosition() == 4)
							GameContext.getBlock()[pin.getPosition()].setTexture(new Texture(Gdx.files.internal("data/block1A.png")));
						else if(pin.getPosition() == 6 || pin.getPosition() == 7)
							GameContext.getBlock()[pin.getPosition()].setTexture(new Texture(Gdx.files.internal("data/block2A.png")));
						else if(pin.getPosition() == 10 || pin.getPosition() == 13)
							GameContext.getBlock()[pin.getPosition()].setTexture(new Texture(Gdx.files.internal("data/block3A.png")));
						else if(pin.getPosition() == 15 || pin.getPosition() == 16)
							GameContext.getBlock()[pin.getPosition()].setTexture(new Texture(Gdx.files.internal("data/block4A.png")));
					}

					else if(GameContext.getBlock()[pin.getPosition()].getTeam() == 2){
						if(pin.getPosition() == 1 || pin.getPosition() == 4)
							GameContext.getBlock()[pin.getPosition()].setTexture(new Texture(Gdx.files.internal("data/block1B.png")));
						else if(pin.getPosition() == 6 || pin.getPosition() == 7)
							GameContext.getBlock()[pin.getPosition()].setTexture(new Texture(Gdx.files.internal("data/block2B.png")));
						else if(pin.getPosition() == 10 || pin.getPosition() == 13)
							GameContext.getBlock()[pin.getPosition()].setTexture(new Texture(Gdx.files.internal("data/block3B.png")));
						else if(pin.getPosition() == 15 || pin.getPosition() == 16)
							GameContext.getBlock()[pin.getPosition()].setTexture(new Texture(Gdx.files.internal("data/block4B.png")));
					}
					
					pin.loseSoldier(this.temp);
				}
				
				WarGame.context.setFunction(new MainMap());
			}
		}
		
		GameContext.getPinA().Draw(batch);
		GameContext.getPinB().Draw(batch);
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
	}
}