package com.me.wargame;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class End implements Function{
	
	public End()
	{		
		GameContext.setTurn(GameContext.getTurn() + 1);
	}

	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void render(SpriteBatch batch) {
		// TODO Auto-generated method stub
		for(int i=0;i<GameContext.getBlockSize();++i)
			GameContext.getBlock()[i].Draw(batch);
		
		GameContext.getPinA().Draw(batch);
		GameContext.getPinB().Draw(batch);
		/*
		if(Smog.getAllPowerTurn() == 0)
		{
			System.out.println("AllBattle");
			
			if(GameContext.getPinA().getSoldier() >= (GameContext.getPinB().getSoldier() * 2)){
				GameContext.getPinA().loseSoldier(GameContext.getPinB().getSoldier());
				GameContext.getPinA().losetotalSoldier(GameContext.getPinB().getSoldier());
				GameContext.getPinB().loseSoldier(GameContext.getPinB().getSoldier());
				GameContext.getPinB().losetotalSoldier(GameContext.getPinB().getSoldier());
			}
			else if(GameContext.getPinB().getSoldier() >= (GameContext.getPinA().getSoldier()*2)){
				GameContext.getPinB().loseSoldier(GameContext.getPinA().getSoldier());
				GameContext.getPinB().losetotalSoldier(GameContext.getPinA().getSoldier());
				GameContext.getPinA().loseSoldier(GameContext.getPinA().getSoldier());
				GameContext.getPinA().losetotalSoldier(GameContext.getPinA().getSoldier());
			}
			
			else if(GameContext.getPinA().getSoldier() > GameContext.getPinB().getSoldier()){
				GameContext.getPinA().loseSoldier(GameContext.getPinB().getSoldier());
				GameContext.getPinA().losetotalSoldier(GameContext.getPinB().getSoldier());
				GameContext.getPinB().loseSoldier(GameContext.getPinB().getSoldier()-1);
				GameContext.getPinB().losetotalSoldier(GameContext.getPinB().getSoldier()-1);
				
			}
			
			else if(GameContext.getPinB().getSoldier() > GameContext.getPinA().getSoldier()){
				GameContext.getPinB().loseSoldier(GameContext.getPinA().getSoldier());
				GameContext.getPinB().losetotalSoldier(GameContext.getPinA().getSoldier());
				GameContext.getPinA().loseSoldier(GameContext.getPinA().getSoldier()-1);
				GameContext.getPinA().losetotalSoldier(GameContext.getPinA().getSoldier()-1);
			}
		}*/

		WarGame.context.setFunction(new MainMap());
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
	}
}