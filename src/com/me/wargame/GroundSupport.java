package com.me.wargame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class GroundSupport implements Function{
	
	private Pin pin;
	private int temp = 0;
	private Texture GroundSupportImage;
	
	private int x = GameContext.convertX(250);
	private int y = GameContext.convertY(50);
	private int width = GameContext.convertX(300);
	private int height = GameContext.convertY(400);
	
	public GroundSupport(Pin pin) {
		// TODO Auto-generated constructor stub
		this.pin = pin;
		GroundSupportImage = new Texture(Gdx.files.internal("data/GroundSupportCard.png"));
	}

	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void render(SpriteBatch batch) {
		batch.draw(GameContext.getMapImage(), 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

		for(int i=0;i<GameContext.getBlockSize();++i)
			GameContext.getBlock()[i].Draw(batch);

		GameContext.getPinA().Draw(batch);
		GameContext.getPinB().Draw(batch);

		if(temp == 0){
			batch.draw(GroundSupportImage, x, y, width, height);
			if(Gdx.input.justTouched()){
				temp = 1;
			}
		}
		else if(temp == 1){
			//���� ���� ������
			pin.setShield(1);
			
			WarGame.context.setFunction(new MainMap());
			temp = 0;
		}
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
	}
}
