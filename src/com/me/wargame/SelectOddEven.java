package com.me.wargame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class SelectOddEven implements Function{
	private Pin pin;
	private Vector2 inputPos;
	private Rectangle oddRect;
	private Rectangle evenRect;

	private Texture dice1;
	private Texture dice2;
	private Texture dice3;
	private Texture dice4;
	private Texture dice5;
	private Texture dice6;
	
	private Texture odd;
	private Texture even;

	private int frame = 0;
	private int temp = 0;
	private int index;
	
	private int x = GameContext.convertX(250);
	private int y = GameContext.convertY(50);
	private int width = GameContext.convertX(300);
	private int height = GameContext.convertY(400);
	
	public SelectOddEven(Pin pin, int index)
	{
		this.pin = pin;
		this.index = index;
		
		inputPos = new Vector2();
				
		dice1 = new Texture(Gdx.files.internal("data/dice1.png"));
		dice2 = new Texture(Gdx.files.internal("data/dice2.png"));
		dice3 = new Texture(Gdx.files.internal("data/dice3.png"));
		dice4 = new Texture(Gdx.files.internal("data/dice4.png"));
		dice5 = new Texture(Gdx.files.internal("data/dice5.png"));
		dice6 = new Texture(Gdx.files.internal("data/dice6.png"));
		
		odd = new Texture(Gdx.files.internal("data/odd.png"));
		even = new Texture(Gdx.files.internal("data/even.png"));
		
		oddRect = new Rectangle(GameContext.convertX(360), GameContext.convertY(195), GameContext.convertX(25), GameContext.convertY(25));
		evenRect = new Rectangle(GameContext.convertX(385), GameContext.convertY(195), GameContext.convertX(25), GameContext.convertY(25));
	}

	int roll(){
		int random=(int)(Math.random()*6 + 1);

		switch(random){
		case 1:
			GameContext.setDiceImage(dice1);
			break;
		case 2:
			GameContext.setDiceImage(dice2);
			break;
		case 3:
			GameContext.setDiceImage(dice3);
			break;
		case 4:
			GameContext.setDiceImage(dice4);
			break;
		case 5:
			GameContext.setDiceImage(dice5);
			break;
		case 6:
			GameContext.setDiceImage(dice6);
			break;
		}
		return random;
	}
	void setTexture(int temp)
	{
		switch(temp){
		case 1:
			GameContext.setDiceImage(dice1);
			break;
		case 2:
			GameContext.setDiceImage(dice2);
			break;
		case 3:
			GameContext.setDiceImage(dice3);
			break;
		case 4:
			GameContext.setDiceImage(dice4);
			break;
		case 5:
			GameContext.setDiceImage(dice5);
			break;
		case 6:
			GameContext.setDiceImage(dice6);
			break;
		}
	}
	
	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;

	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void render(SpriteBatch batch) {
		// TODO Auto-generated method stub

		for(int i=0;i<GameContext.getBlockSize();++i)
			GameContext.getBlock()[i].Draw(batch);
		
		GameContext.getPinA().Draw(batch);
		GameContext.getPinB().Draw(batch);

		if(temp == 0){
			System.out.println("Dicing");
			
			GameContext.setDice(roll());
			setTexture(GameContext.getDice());
			batch.draw(GameContext.getDiceImage(), x, y, width, height);
			batch.draw(odd, GameContext.convertX(360), GameContext.convertY(195), GameContext.convertX(25), GameContext.convertY(25));
			batch.draw(even, GameContext.convertX(385), GameContext.convertY(195), GameContext.convertX(25), GameContext.convertY(25));
			
			if(Gdx.input.justTouched()){
				inputPos.x = Gdx.input.getX();
				inputPos.y = Gdx.graphics.getHeight() - Gdx.input.getY();
				
				if(oddRect.contains(inputPos.x, inputPos.y))
				{
					Smog.setNum(1);	//Ȧ�� ����
					temp = 1;
				}
				else if(evenRect.contains(inputPos.x, inputPos.y))
				{
					Smog.setNum(2);	//¦�� ����
					temp = 1;
				}
			}
		}

		else if(temp == 1){
			batch.draw(GameContext.getDiceImage(), x, y, width, height);
			frame++;
			if(frame > 20 || Gdx.input.justTouched()){
				temp = 2;
			}
		}
		else if(temp == 2){
			if((GameContext.getDice()%2 == 1 && Smog.getNum() == 1) || (GameContext.getDice()%2 == 0 && Smog.getNum() == 2))
			{
				if(pin == GameContext.getPinA())
				{
					GameContext.getBlock()[index].station(1, 0);
				}
				else if(pin == GameContext.getPinB())
				{
					GameContext.getBlock()[index].station(2, 0);
				}
			}
				
			if(GameContext.getBlock()[index].getTeam() == 1){
				if(index == 1 || index == 4)
					GameContext.getBlock()[index].setTexture(new Texture(Gdx.files.internal("data/block1A.png")));
				else if(index == 6 || index == 7)
					GameContext.getBlock()[index].setTexture(new Texture(Gdx.files.internal("data/block2A.png")));
				else if(index == 10 || index == 13)
					GameContext.getBlock()[index].setTexture(new Texture(Gdx.files.internal("data/block3A.png")));
				else if(index == 15 || index == 16)
					GameContext.getBlock()[index].setTexture(new Texture(Gdx.files.internal("data/block4A.png")));
			}

			else if(GameContext.getBlock()[index].getTeam() == 2){
				if(index == 1 || index == 4)
					GameContext.getBlock()[index].setTexture(new Texture(Gdx.files.internal("data/block1B.png")));
				else if(index == 6 || index == 7)
					GameContext.getBlock()[index].setTexture(new Texture(Gdx.files.internal("data/block2B.png")));
				else if(index == 10 || index == 13)
					GameContext.getBlock()[index].setTexture(new Texture(Gdx.files.internal("data/block3B.png")));
				else if(index == 15 || index == 16)
					GameContext.getBlock()[index].setTexture(new Texture(Gdx.files.internal("data/block4B.png")));
			}

			WarGame.context.setFunction(new MainMap());
			frame = 0;
			temp = 0;
		}
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
	}
}
