package com.me.wargame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

/**
 * @author KimSeungYoon
 * ���� ���
 */
public class Tunnel implements Function{
	
	private Texture TunnelImage;
	private Texture TunnelUse;
	private Texture TunnelUnUse;
	private Rectangle Use;
	private Rectangle UnUse;
	private Pin pin;
	
	public Tunnel(Pin pin)
	{
		this.pin = pin;
		
		TunnelImage = new Texture(Gdx.files.internal("data/TunnelCard.png"));
		TunnelUse = new Texture(Gdx.files.internal("data/TunnelUse.png"));
		TunnelUnUse = new Texture(Gdx.files.internal("data/TunnelUnUse.png"));
		
		Use = new Rectangle(GameContext.convertX(300), GameContext.convertY(270), GameContext.convertX(100), GameContext.convertY(30));
		UnUse = new Rectangle(GameContext.convertX(400), GameContext.convertY(270), GameContext.convertX(100), GameContext.convertY(30));
	}

	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		System.out.println(screenX);
		System.out.println(screenY);
		System.out.println(Use.contains(screenX, screenY));
		
		if(Use.contains(screenX, screenY))
		{
			if(pin.getPosition() == 2)
				pin.setPosition(11);
			else if(pin.getPosition() == 11)
				pin.setPosition(2);
			
			WarGame.context.setFunction(new MainMap());
		}
		else if(UnUse.contains(screenX, screenY))
		{
			WarGame.context.setFunction(new MainMap());
		}
		
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void render(SpriteBatch batch) {
		// TODO Auto-generated method stub
		//System.out.println("Tunnel");
		batch.draw(GameContext.getMapImage(), 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

		for(int i=0;i<GameContext.getBlockSize();++i)
			GameContext.getBlock()[i].Draw(batch);
		
		batch.draw(TunnelImage, GameContext.convertX(300), GameContext.convertY(210));
		batch.draw(TunnelUse, GameContext.convertX(300), GameContext.convertY(180));
		batch.draw(TunnelUnUse, GameContext.convertX(400), GameContext.convertY(180));

		GameContext.getPinA().Draw(batch);
		GameContext.getPinB().Draw(batch);
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		TunnelImage.dispose();
		TunnelUse.dispose();
		TunnelUnUse.dispose();
	}	
}
