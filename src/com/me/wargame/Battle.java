package com.me.wargame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class Battle implements Function{
	
	private Pin pin;
	private int blockSoldier;
	
	Battle(Pin pin)
	{
		this.pin = pin;
	}

	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void render(SpriteBatch batch) {
		// TODO Auto-generated method stub
		for(int i=0;i<GameContext.getBlockSize();++i)
			GameContext.getBlock()[i].Draw(batch);

		if(pin == GameContext.getPinA())
		{
			blockSoldier = GameContext.getBlock()[pin.getPosition()].getSoldier();
			
			//���޻��� 1
			if(pin.getTurn() != 0)
			{
				GameContext.getBlock()[pin.getPosition()].battle(pin.getSoldier());
				
				//pin.loseSoldier(blockSoldier * 2);
				pin.setTurn(pin.getTurn()-1);
			}
			else if(pin.getTurn() == 0)
			{
				GameContext.getBlock()[pin.getPosition()].battle(pin.getSoldier());
			}
	
			//��������
			if(pin.getShield() == 0)
			{
				pin.loseSoldier((int) (blockSoldier*1.5));
				pin.losetotalSoldier((int) (blockSoldier*1.5));
			}
			else if(pin.getShield() == 1)
				pin.setShield(0);
				
			GameContext.getPinB().losetotalSoldier(blockSoldier);
		}
		else if(pin == GameContext.getPinB())
		{
			blockSoldier = GameContext.getBlock()[pin.getPosition()].getSoldier();
			
			//���޻��� 1
			if(pin.getTurn() != 0)
			{
				GameContext.getBlock()[pin.getPosition()].battle(pin.getSoldier()*2);
				pin.setTurn(pin.getTurn()-1);
			}
			else if(pin.getTurn() == 0)
			{
				GameContext.getBlock()[pin.getPosition()].battle(pin.getSoldier());
			}
	
			//��������
			if(pin.getShield() == 0)
			{
				pin.loseSoldier((int) (blockSoldier*1.5));
				pin.losetotalSoldier((int) (blockSoldier*1.5));
			}
			else if(pin.getShield() == 1)
				pin.setShield(0);

			GameContext.getPinA().losetotalSoldier(blockSoldier);
		}
		
		if(GameContext.getBlock()[pin.getPosition()].getTeam()==0){
			if(pin.getPosition() == 1 || pin.getPosition() == 4)
				GameContext.getBlock()[pin.getPosition()].setTexture(new Texture(Gdx.files.internal("data/block1.png")));
			else if(pin.getPosition() == 6 || pin.getPosition() == 7)
				GameContext.getBlock()[pin.getPosition()].setTexture(new Texture(Gdx.files.internal("data/block2.png")));
			else if(pin.getPosition() == 10 || pin.getPosition() == 13)
				GameContext.getBlock()[pin.getPosition()].setTexture(new Texture(Gdx.files.internal("data/block3.png")));
			else if(pin.getPosition() == 15 || pin.getPosition() == 16)
				GameContext.getBlock()[pin.getPosition()].setTexture(new Texture(Gdx.files.internal("data/block4.png")));
		}
		
		GameContext.getPinA().Draw(batch);
		GameContext.getPinB().Draw(batch);
		
		
		if(GameContext.getPinA().getPosition() == GameContext.getPinB().getPosition()){
			System.out.println("AllBattle");
			WarGame.context.setFunction(new AllBattle(pin));
		}
		else
			WarGame.context.setFunction(new MainMap());		
	}
	

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
	}
}