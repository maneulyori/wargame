package com.me.wargame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

public class AirBomb implements Function{
	
	private int temp;
	private Vector2 inputPos;
	private Texture AirBombImage;
	
	private int x = GameContext.convertX(250);
	private int y = GameContext.convertY(50);
	private int width = GameContext.convertX(300);
	private int height = GameContext.convertY(400);
	
	public AirBomb()
	{
		inputPos = new Vector2();
		AirBombImage = new Texture(Gdx.files.internal("data/AirBombCard.png"));
	}

	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub		

		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void render(SpriteBatch batch) {
		batch.draw(GameContext.getMapImage(), 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

		for(int i=0;i<GameContext.getBlockSize();++i)
			GameContext.getBlock()[i].Draw(batch);		
		
		GameContext.getPinA().Draw(batch);
		GameContext.getPinB().Draw(batch);
		
		if(temp == 0){
			if(Gdx.input.justTouched()){
				batch.draw(AirBombImage, x, y, width, height);
				
				temp = 1;
			}
		}
		else if(temp == 1){
			if(Gdx.input.justTouched()){
				inputPos.x = Gdx.input.getX();
				inputPos.y = Gdx.graphics.getHeight() - Gdx.input.getY();
				
				int i;
				
				for(i=0; i<GameContext.buildingPos.length; i++) {
					if (GameContext.getBlock()[i].getRectangle().contains(inputPos.x, inputPos.y))
					{
						int loose = GameContext.getBlock()[i].getSoldier() / 2;
						GameContext.getBlock()[i].setSoldier(GameContext.getBlock()[i].getSoldier() - loose);
						if(GameContext.getBlock()[i].getTeam() == 1)
						{
							GameContext.getPinA().losetotalSoldier(loose);
						}
						else
						{
							GameContext.getPinB().losetotalSoldier(loose);
						}
					}
				}
				
				WarGame.context.setFunction(new MainMap());
				temp = 0;
			}
		}
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
	}
}