package com.me.wargame;

import java.net.DatagramSocket;
import java.net.InetAddress;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class WarGame implements ApplicationListener {
	private SpriteBatch batch;
	
	public static GameContext context;
	private int frame = 0;
	DatagramSocket serverSocket;
	GameServer gameServer;
	AndroidNativeInterface aInterface = null;
	int myPin = 2;
	
	public WarGame(AndroidNativeInterface mainActivity) {
		this.aInterface = mainActivity;
	}
	
	public WarGame() {
	}

	@Override
	public void create() {				
		batch = new SpriteBatch();
		
		context = new GameContext(this);
		context.setFunction(new MainMap());
		
		System.out.println("Screen Res : " + Integer.toString(Gdx.graphics.getWidth()) + "*" + Integer.toString(Gdx.graphics.getHeight()));
	}
	
	@Override
	public void dispose() {
		batch.dispose();
	}

	@Override
	public void render() {		
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		
		batch.begin();
		
		if(GameContext.getPinA().getSoldier() >= 0 && GameContext.getPinB().getSoldier() >= 0)
			WarGame.context.getFunction().render(batch);
		else{
			if(frame<100){
				batch.draw(new Texture(Gdx.files.internal("data/END.png")), 0, 0);
				frame++;
			}
			else if(frame>=100){
				Gdx.app.exit();
			}
		}
		batch.end();
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}
	
	public void createServer(int port)
	{
		myPin = 1;
		if(aInterface != null)
			aInterface.openWiFiDirect();
		
		gameServer = new GameServer(port, this);
		
		//Start listener thread
		gameServer.start();
	}
	
	public void createClient(InetAddress host, int port)
	{
		myPin = 2;
		gameServer = new GameServer(host, port, this);
	}

	public void broadcast(String message) {
		if(gameServer != null) {
			gameServer.sendData(message);
		}
	}
	
	public void messageHandler(String[] message) {
		
		if(message[0].equals("POSITION_CHANGE")) {
			int pin_data = Integer.parseInt(message[1]);
			
			if(pin_data != myPin)
			{
				Pin pin;
				if(pin_data == 1 )
				{
					pin = GameContext.getPinA(); 
				}
				else
				{
					pin = GameContext.getPinB();
				}
				
				pin.setPosition(Integer.parseInt(message[2]));
			}
		}
		else if(message.equals("TURN_CHANGE")) {
			GameContext.setTurn(Integer.parseInt(message[1])-1); 
			GameContext.setMode(4); //Set mode to End
			WarGame.context.setFunction(new MainMap());
		}
	}
}