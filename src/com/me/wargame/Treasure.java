package com.me.wargame;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Treasure implements Function{
	
	private Pin pin;
	private Rectangle rect;
	private Vector2 inputPos;
	private int random;
	private int temp = 0;
	private static int WarPause = 0;

	private Texture AttackImage;
	private Texture AddSoldierImage;
	private Texture AirSupportImage;
	private Texture MarchImage;
	private Texture TrapImage;
	private Texture PauseImage;
	
	private Texture dice1;
	private Texture dice2;
	private Texture dice3;
	private Texture dice4;
	private Texture dice5;
	private Texture dice6;

	private Rectangle DiceRect1;
	private Rectangle DiceRect2;
	private Rectangle DiceRect3;
	private Rectangle DiceRect4;
	private Rectangle DiceRect5;
	private Rectangle DiceRect6;
	
	private int x = GameContext.convertX(250);
	private int y = GameContext.convertY(50);
	private int width = GameContext.convertX(300);
	private int height = GameContext.convertY(400);
	
	public Treasure(Pin pin)
	{
		this.pin = pin;
		inputPos = new Vector2();
		
		AttackImage = new Texture(Gdx.files.internal("data/AttackCard.png"));
		AddSoldierImage = new Texture(Gdx.files.internal("data/AddSoldierCard.png"));
		AirSupportImage = new Texture(Gdx.files.internal("data/AirSupportCard.png"));
		MarchImage = new Texture(Gdx.files.internal("data/MarchCard.png"));
		TrapImage= new Texture(Gdx.files.internal("data/TrapCard.png"));
		PauseImage = new Texture(Gdx.files.internal("data/PauseCard.png"));
	
		dice1 = new Texture(Gdx.files.internal("data/dice1.png"));
		dice2 = new Texture(Gdx.files.internal("data/dice2.png"));
		dice3 = new Texture(Gdx.files.internal("data/dice3.png"));
		dice4 = new Texture(Gdx.files.internal("data/dice4.png"));
		dice5 = new Texture(Gdx.files.internal("data/dice5.png"));
		dice6 = new Texture(Gdx.files.internal("data/dice6.png"));
		
		int width = GameContext.convertX(50);
		int height = GameContext.convertY(50);
		
		DiceRect1 = new Rectangle(GameContext.convertX(300), GameContext.convertY(250), width, height);
		DiceRect2 = new Rectangle(GameContext.convertX(360), GameContext.convertY(250), width, height);
		DiceRect3 = new Rectangle(GameContext.convertX(420), GameContext.convertY(250), width, height);
		DiceRect4 = new Rectangle(GameContext.convertX(300), GameContext.convertY(190), width, height);
		DiceRect5 = new Rectangle(GameContext.convertX(360), GameContext.convertY(190), width, height);
		DiceRect6 = new Rectangle(GameContext.convertX(420), GameContext.convertY(190), width, height);
		
		random=(int)(Math.random()*6 + 1);
	}
	
	public void setRectangle(Rectangle rect)
	{
		this.rect = rect;
	}
	
	public Rectangle getRectangle()
	{
		return rect;
	}

	public static void setWarPause(int WarPause)
	{
		Treasure.WarPause = WarPause;
	}
	
	public static int getWarPause()
	{
		return Treasure.WarPause;
	}

	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void render(SpriteBatch batch) {
		batch.draw(GameContext.getMapImage(), 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		
		for(int i=0;i<GameContext.getBlockSize();++i)
			GameContext.getBlock()[i].Draw(batch);
		
		GameContext.getPinA().Draw(batch);
		GameContext.getPinB().Draw(batch);

		if (GameContext.getBlock()[pin.getPosition()].getTrap() == 1)
		{
			pin.loseSoldier(100);
			pin.losetotalSoldier(100);
			GameContext.getBlock()[pin.getPosition()].setTrap(0);
			GameContext.getBlock()[pin.getPosition()].setTexture(new Texture(Gdx.files
					.internal("data/treasure.png")));
		}
		
		if(random == 1)
		{
			if(temp == 0){
				batch.draw(AttackImage, x, y, width, height);
				if(Gdx.input.justTouched()){
					temp = 1;
				}
			}
			else if(temp == 1){
				pin.setTurn(2);
				WarGame.context.setFunction(new MainMap());
				temp = 0;
			}
		}
		else if(random == 2)
		{
			if(temp == 0){
				batch.draw(AddSoldierImage, x, y, width, height);
				if(Gdx.input.justTouched()){
					temp = 1;
				}
			}
			else if(temp == 1){
				pin.earnSoldier(100);
				WarGame.context.setFunction(new MainMap());
				temp = 0;
			}
		}
		else if(random == 3)
		{
			if(temp == 0){
				batch.draw(AirSupportImage, x, y, width, height);
				if(Gdx.input.justTouched()){
					temp = 1;
				}
			}
			else if(temp == 1){
				if(Gdx.input.justTouched()){
					inputPos.x = Gdx.input.getX();
					inputPos.y = Gdx.graphics.getHeight() - Gdx.input.getY();

					//�̹� ������ �ִ� ��� Ȥ�� ������ ���� ���� ��� ��� �����ؾ���
					if(GameContext.getBlock()[1].getPolygon().contains(inputPos.x, inputPos.y))
					{
						
						//���� �� �� ������ ���������� �����ϰԲ�
						if(GameContext.getPinA().getPosition() == 5){
							GameContext.getBlock()[1].station(1, 100);
							GameContext.getBlock()[1].setTexture(new Texture(Gdx.files.internal("data/block1A.png")));
						}
						else if(GameContext.getPinB().getPosition() == 5){
							GameContext.getBlock()[1].station(2, 100);
							GameContext.getBlock()[1].setTexture(new Texture(Gdx.files.internal("data/block1B.png")));
						}
						
						temp = 0;
						WarGame.context.setFunction(new MainMap());
					}
					else if(GameContext.getBlock()[4].getPolygon().contains(inputPos.x, inputPos.y))
					{
						//���� �� �� ������ ���������� �����ϰԲ�
						if(GameContext.getPinA().getPosition() == 5){
							GameContext.getBlock()[4].station(1, 100);
							GameContext.getBlock()[4].setTexture(new Texture(Gdx.files.internal("data/block1A.png")));
						}
						else if(GameContext.getPinB().getPosition() == 5){
							GameContext.getBlock()[4].station(2, 100);
							GameContext.getBlock()[4].setTexture(new Texture(Gdx.files.internal("data/block1B.png")));
						}
						
						temp = 0;
						WarGame.context.setFunction(new MainMap());
					}
					else if(GameContext.getBlock()[6].getPolygon().contains(inputPos.x, inputPos.y))
					{
						//���� �� �� ������ ���������� �����ϰԲ�
						if(GameContext.getPinA().getPosition() == 5){
							GameContext.getBlock()[6].station(1, 100);
							GameContext.getBlock()[6].setTexture(new Texture(Gdx.files.internal("data/block2A.png")));
						}
						else if(GameContext.getPinB().getPosition() == 5){
							GameContext.getBlock()[6].station(2, 100);
							GameContext.getBlock()[6].setTexture(new Texture(Gdx.files.internal("data/block2B.png")));
						}
						
						temp = 0;
						WarGame.context.setFunction(new MainMap());
					}
					else if(GameContext.getBlock()[7].getPolygon().contains(inputPos.x, inputPos.y))
					{
						//���� �� �� ������ ���������� �����ϰԲ�
						if(GameContext.getPinA().getPosition() == 5){
							GameContext.getBlock()[7].station(1, 100);
							GameContext.getBlock()[7].setTexture(new Texture(Gdx.files.internal("data/block2A.png")));
						}
						else if(GameContext.getPinB().getPosition() == 5){
							GameContext.getBlock()[7].station(2, 100);
							GameContext.getBlock()[7].setTexture(new Texture(Gdx.files.internal("data/block2B.png")));
						}
						
						temp = 0;
						WarGame.context.setFunction(new MainMap());
					}
					else if(GameContext.getBlock()[10].getPolygon().contains(inputPos.x, inputPos.y))
					{
						//���� �� �� ������ ���������� �����ϰԲ�
						if(GameContext.getPinA().getPosition() == 5){
							GameContext.getBlock()[10].station(1, 100);
							GameContext.getBlock()[10].setTexture(new Texture(Gdx.files.internal("data/block3A.png")));
						}
						else if(GameContext.getPinB().getPosition() == 5){
							GameContext.getBlock()[10].station(2, 100);
							GameContext.getBlock()[10].setTexture(new Texture(Gdx.files.internal("data/block3B.png")));
						}
						
						temp = 0;
						WarGame.context.setFunction(new MainMap());
					}
					else if(GameContext.getBlock()[13].getPolygon().contains(inputPos.x, inputPos.y))
					{
						//���� �� �� ������ ���������� �����ϰԲ�
						if(GameContext.getPinA().getPosition() == 5){
							GameContext.getBlock()[13].station(1, 100);
							GameContext.getBlock()[13].setTexture(new Texture(Gdx.files.internal("data/block3A.png")));
						}
						else if(GameContext.getPinB().getPosition() == 5){
							GameContext.getBlock()[13].station(2, 100);
							GameContext.getBlock()[13].setTexture(new Texture(Gdx.files.internal("data/block3B.png")));
						}
						
						temp = 0;
						WarGame.context.setFunction(new MainMap());
					}
					else if(GameContext.getBlock()[15].getPolygon().contains(inputPos.x, inputPos.y))
					{
						//���� �� �� ������ ���������� �����ϰԲ�
						if(GameContext.getPinA().getPosition() == 5){
							GameContext.getBlock()[15].station(1, 100);
							GameContext.getBlock()[15].setTexture(new Texture(Gdx.files.internal("data/block4A.png")));
						}
						else if(GameContext.getPinB().getPosition() == 5){
							GameContext.getBlock()[15].station(2, 100);
							GameContext.getBlock()[15].setTexture(new Texture(Gdx.files.internal("data/block4B.png")));
						}
						
						temp = 0;
						WarGame.context.setFunction(new MainMap());
					}
					else if(GameContext.getBlock()[16].getPolygon().contains(inputPos.x, inputPos.y))
					{
						//���� �� �� ������ ���������� �����ϰԲ�
						if(GameContext.getPinA().getPosition() == 5){
							GameContext.getBlock()[16].station(1, 100);
							GameContext.getBlock()[16].setTexture(new Texture(Gdx.files.internal("data/block4A.png")));
						}
						else if(GameContext.getPinB().getPosition() == 5){
							GameContext.getBlock()[16].station(2, 100);
							GameContext.getBlock()[16].setTexture(new Texture(Gdx.files.internal("data/block4B.png")));
						}
						
						temp = 0;
						WarGame.context.setFunction(new MainMap());
					}
				}
			}
		}
		else if(random == 4)
		{
			if(temp == 0){
				batch.draw(MarchImage, x, y, width, height);
				
				if(Gdx.input.justTouched()){
					temp = 1;
				}
			}
			else if(temp == 1){
				//�ֻ��� �� �߰��� �׸���
				
				int width = GameContext.convertX(50);
				int height = GameContext.convertY(50);
				
				batch.draw(dice1, GameContext.convertX(300), GameContext.convertY(250), width, height);
				batch.draw(dice2, GameContext.convertX(360), GameContext.convertY(250), width, height);
				batch.draw(dice3, GameContext.convertX(420), GameContext.convertY(250), width, height);
				batch.draw(dice4, GameContext.convertX(300), GameContext.convertY(190), width, height);
				batch.draw(dice5, GameContext.convertX(360), GameContext.convertY(190), width, height);
				batch.draw(dice6, GameContext.convertX(420), GameContext.convertY(190), width, height);
				
				if(Gdx.input.justTouched()){
					inputPos.x = Gdx.input.getX();
					inputPos.y = Gdx.graphics.getHeight() - Gdx.input.getY();
					
					if(DiceRect1.contains(inputPos.x, inputPos.y))
					{
						pin.setPredice(1);
						
						WarGame.context.setFunction(new MainMap());
						temp = 0;
					}
					else if(DiceRect2.contains(inputPos.x, inputPos.y))
					{
						pin.setPredice(2);
						
						WarGame.context.setFunction(new MainMap());
						temp = 0;
					}
					else if(DiceRect3.contains(inputPos.x, inputPos.y))
					{
						pin.setPredice(3);
						
						WarGame.context.setFunction(new MainMap());
						temp = 0;
					}
					else if(DiceRect4.contains(inputPos.x, inputPos.y))
					{
						pin.setPredice(4);
						
						WarGame.context.setFunction(new MainMap());
						temp = 0;
					}
					else if(DiceRect5.contains(inputPos.x, inputPos.y))
					{
						pin.setPredice(5);
						
						WarGame.context.setFunction(new MainMap());
						temp = 0;
					}
					else if(DiceRect6.contains(inputPos.x, inputPos.y))
					{
						pin.setPredice(6);
						
						WarGame.context.setFunction(new MainMap());
						temp = 0;
					}
				}				
			}
		}
		else if(random == 5)
		{			
			if(temp == 0){
				batch.draw(TrapImage, x, y, width, height);
				if(Gdx.input.justTouched()){
					temp = 1;
				}
			}
			else if(temp == 1){
				if (GameContext.getBlock()[pin.getPosition()].getTrap() == 0) {
					GameContext.getBlock()[pin.getPosition()].setTrap(1);
					GameContext.getBlock()[pin.getPosition()].setTexture(new Texture(Gdx.files.internal("data/Trap.png")));
				}

				WarGame.context.setFunction(new MainMap());
				temp = 0;
			}
		}
		else if(random == 6)
		{
			//�Ͻ� ����
			if(temp == 0){
				batch.draw(PauseImage, x, y, width, height);
				if(Gdx.input.justTouched()){
					temp =1;
				}
			}
			else if(temp == 1){
				//����
				Treasure.setWarPause(1);
				
				WarGame.context.setFunction(new MainMap());
				temp = 0;
			}
		}
	}
	
	@Override
	public void dispose() {
		// TODO Auto-generated method stub
	}
}
