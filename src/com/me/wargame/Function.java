package com.me.wargame;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;


public interface Function extends InputProcessor {
	public void render(SpriteBatch batch);
	public void dispose();
}
