package com.me.wargame;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.graphics.g2d.BitmapFont;

public class Pin{
	private int position;
	private int soldier;
	private int totalsoldier;
	private int team;

	private int turn = 0;
	private int shield = 0;	//���� ����
	private int predice = 0;	//�?�� �౺
	private int direction = 1;
	private int BackCheck = 0;	//�Ĺ� Ȯ��
	private int Greed = 0;	//������ �������

	private Texture texture;
	private Texture teamtexture;
	private Rectangle rect;
    private BitmapFont font;
    private WarGame wargame;
	
	Pin(Texture texture, Texture teamtexture, int team, WarGame wargame)
	{
		this.texture=texture;
		this.teamtexture=teamtexture;
		this.team = team;
		this.wargame = wargame;
		
		font = new BitmapFont();
	}
	
	public void setBackCheck(int BackCheck)
	{
		this.BackCheck = BackCheck;
	}
	
	public int getBackCheck()
	{
		return BackCheck;
	}
	
	public void setGreed(int Greed)
	{
		this.Greed = Greed;
		for (StackTraceElement ste : Thread.currentThread().getStackTrace()) {
		    System.out.println(ste);
		}
	}
	
	public int getGreed()
	{
		return Greed;
	}

	public void Spawn(Rectangle paramRect)
	{
		rect = paramRect;
	}
		
	public Rectangle GetRectangle()
	{
		return rect;
	}
	void setShield(int shield)
	{
		this.shield=shield;
	}
	int getShield()
	{
		return shield;
	}
	void setTurn(int turn)
	{
		this.turn=turn;
	}
	int getTurn()
	{
		return turn;
	}
	void setPredice(int predice)
	{
		this.predice=predice;
	}
	int getPredice()
	{
		return predice;
	}
	void setPosition(int position)
	{
		this.position=position;
		wargame.broadcast("POSITION_CHANGE " + Integer.toString(team) + " " + Integer.toString(position));
	}
	int getPosition()
	{
		return position;
	}
	
	void setSoldier(int soldier)
	{
		this.soldier=soldier;
		this.totalsoldier=soldier;
		
		System.out.println("setSoldier " + this.soldier);
	}
	void loseSoldier(int soldier){
		this.soldier-=soldier;
		System.out.println("loseSoldier " + this.soldier);
	}
	void losetotalSoldier(int soldier){
		this.totalsoldier-=soldier;
	}
	void earnSoldier(int soldier){
		this.soldier+=soldier;
		this.totalsoldier+=soldier;
		System.out.println("EarnSoldier " + Integer.toString(this.soldier));
	}
	int getSoldier()
	{
		return soldier;
	}
	Texture getTexture()
	{
		return texture;
	}
	int getTeam()
	{
		return team;
	}
	int getDirection(){
		return direction;
	}
	void changeDirection(){
		//TODO: send direction information through network
		direction = (direction * (-1));
	}
	public void Draw(SpriteBatch spriteBatch)
	{
	    font.setColor(Color.BLACK);
	    
	    //������ ���� �䱸
		spriteBatch.draw(texture, rect.x, rect.y, rect.width + 50, rect.height+50);
		
		if(team == 1){
			spriteBatch.draw(teamtexture, 0, GameContext.convertX(280), GameContext.convertY(200), GameContext.convertX(200));
			font.draw(spriteBatch, Integer.toString(this.soldier), GameContext.convertX(100), GameContext.convertY(350));
			font.draw(spriteBatch, Integer.toString(this.totalsoldier), GameContext.convertX(100), GameContext.convertY(405));
		}
		else if(team == 2){
			spriteBatch.draw(teamtexture, GameContext.convertX(600), GameContext.convertY(-20), GameContext.convertX(200), GameContext.convertY(200));
			font.draw(spriteBatch, Integer.toString(this.soldier), GameContext.convertX(700), GameContext.convertY(50));
			font.draw(spriteBatch, Integer.toString(this.totalsoldier), GameContext.convertX(700), GameContext.convertY(105));
		}
	}
}