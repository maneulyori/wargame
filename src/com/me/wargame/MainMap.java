package com.me.wargame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class MainMap implements Function{

	//���
	private int Dicing = 1;
	private int Move = 2;
	private int Function = 3;
	private int End = 4;
	private int None = 0;
	
	private Pin pin;
	private int i;

	private int x = GameContext.convertX(360);
	private int y = GameContext.convertY(220);
	private int width = GameContext.convertX(50);
	private int height = GameContext.convertY(50);
	
	public MainMap()
	{
		i = 0;
				
		//A��
		if(GameContext.getTurn() % 2 == 1)
			pin = GameContext.getPinA();		
		//B��
		else if(GameContext.getTurn() % 2 == 0)
			pin = GameContext.getPinB();
	}
	
	public void rule(Pin pin)
	{
		if(GameContext.getPinA().getPosition() == GameContext.getPinB().getPosition()){
			//System.out.println("AllBattle");
			WarGame.context.setFunction(new Battle(pin));
		}
	
		else if(pin.getPosition()==1 || pin.getPosition()==4 || 
				pin.getPosition()==6 || pin.getPosition()==7 || 
				pin.getPosition()==10 || pin.getPosition()==13 || 
				pin.getPosition()==15 || pin.getPosition()==16){
			//A��
			if(pin == GameContext.getPinA())
			{
				if(!GameContext.getBlock()[pin.getPosition()].isStation() || GameContext.getBlock()[pin.getPosition()].getTeam()==1){
					//System.out.println("Station");
					WarGame.context.setFunction(new SetSoldier(pin));
				}				
				else if(GameContext.getBlock()[pin.getPosition()].getTeam()==2 && Treasure.getWarPause() == 0){
					//System.out.println("Battle");
					WarGame.context.setFunction(new Battle(pin));
				}
				else
					Treasure.setWarPause(0);
			}
			//B��
			else if(pin == GameContext.getPinB())
			{
				if(!GameContext.getBlock()[pin.getPosition()].isStation() || GameContext.getBlock()[pin.getPosition()].getTeam()==2){
					//System.out.println("Station");
					WarGame.context.setFunction(new SetSoldier(pin));
				}				
				else if(GameContext.getBlock()[pin.getPosition()].getTeam()==1 && Treasure.getWarPause() == 0){
					//System.out.println("Battle");
					WarGame.context.setFunction(new Battle(pin));
				}
				else
					Treasure.setWarPause(0);
			}
		}
		
		else if(pin.getPosition() == 3 || pin.getPosition() == 12)
		{
			//System.out.println("Treasure");
			
			WarGame.context.setFunction(new Treasure(pin));
		}
		else if(pin.getPosition() == 5)
		{						
			//System.out.println("AirSupport");
			
			WarGame.context.setFunction(new AirBomb());
		}
		else if(pin.getPosition() == 14)
		{
			//System.out.println("GroundSupport");

			WarGame.context.setFunction(new GroundSupport(pin));
		}
		else if(pin.getPosition() == 8 || pin.getPosition() == 17)
		{
			//System.out.println("Smog");
			
			WarGame.context.setFunction(new Smog(pin));
		}
		else if(pin.getPosition() == 2 || pin.getPosition() == 11)
		{
			//System.out.println("Tunnel");

			WarGame.context.setFunction(new Tunnel(pin));
		}
		else
		{
		}
	}
	
	public void move(Pin pin)
	{
		if(i < GameContext.getDice()*10)
		{
			if(i%10==0){
				if((pin.getPosition() + pin.getDirection()) < 0){
					pin.setPosition((pin.getPosition() + pin.getDirection() + 18));
				}
				else{
					pin.setPosition((pin.getPosition() + pin.getDirection()) % 18);
				}
			}
			i++;
		}
		else
		{
			if(pin.getBackCheck() == 1)
			{
				pin.changeDirection();
				pin.setBackCheck(0);
			}
			
			GameContext.setMode(Function);
		}		
	}

	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub		

		if(Smog.getAllPowerTurn() > 0)
			Smog.setAllPowerTurn(Smog.getAllPowerTurn() - 1);			
		
		GameContext.setMode(Move);

		WarGame.context.setFunction(new Dicing(pin));

		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void render(SpriteBatch batch) {
		// TODO Auto-s method stub
		//System.out.println("MainMap");
		
		batch.draw(GameContext.getMapImage(), 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
				
		for(int i=0;i<GameContext.getBlockSize();++i)
			GameContext.getBlock()[i].Draw(batch);
				
		if(pin.getGreed() == 1)
		{
			pin.setGreed(0);
			GameContext.setMode(End);
		}

		if(GameContext.getMode() == Dicing)
		{			
			//System.out.println("Dicing");
		}
		else if(GameContext.getMode() == Move)
		{
			//System.out.println("Move");
			
			move(pin);
		}
		else if(GameContext.getMode() == Function)
		{
			//System.out.println("Function");
			
			if(pin.getGreed() > 1)
			{
				System.out.println(pin.getGreed());
				GameContext.setMode(Dicing);
				pin.setGreed(pin.getGreed() - 1);
			}
			else				
				GameContext.setMode(End);
			
			rule(pin);
		}
		else if(GameContext.getMode() == End)
		{
			//System.out.println("End");
			GameContext.setMode(None);
			WarGame.context.setFunction(new End());
		}
		else
		{			
		}
				
		GameContext.getPinA().Spawn(GameContext.getBlock()[GameContext.getPinA().getPosition()].getArectangle());
		GameContext.getPinB().Spawn(GameContext.getBlock()[GameContext.getPinB().getPosition()].getBrectangle());

		GameContext.getPinA().Draw(batch);
		GameContext.getPinB().Draw(batch);

		batch.draw(GameContext.getDiceImage(), x, y, width, height);
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
	}
}
