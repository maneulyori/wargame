package com.me.wargame;

import java.io.*;
import java.net.*;

public class GameServer extends Thread{
	
	DatagramSocket serverSocket;
	DatagramPacket receivePacket;
	int port;
	int clientPort;
	InetAddress clientAddress;
	InetAddress serverAddress;
	WarGame wargame;
	boolean isClient;
	
	GameServer(int port, WarGame wargame) {
		this.port = port;
		this.wargame = wargame;
		this.isClient = false;
		
		try {
			serverSocket = new DatagramSocket(port);
		} catch (SocketException e) {
			//TODO: Socket creation failed. report it to user.
			e.printStackTrace();
		}
	}
	
	GameServer(InetAddress serverAddress, int port, WarGame wargame) {
		this.port = port;
		this.wargame = wargame;
		this.isClient = true;
		this.serverAddress = serverAddress;
		System.out.println("Starting client...");
		
		try {
			serverSocket = new DatagramSocket();
		} catch (SocketException e) {
			// TODO: report to user.
			e.printStackTrace();
		}
		
		sendData("HELLO"); //Say HELLO to server.
	}
	
	public void run() {
		for(;;)
		{
			byte[] receiveData = new byte[1024];
			receivePacket = new DatagramPacket(receiveData, receiveData.length);
		
			try {
				serverSocket.receive(receivePacket);
			} catch (IOException e) {
				// Failed to receive packet.
				e.printStackTrace();
			}
			
			int i;
			StringBuilder builder = new StringBuilder("");
			
			for (i=0; i<receivePacket.getData().length; i++)
			{
				if(receivePacket.getData()[i] == 0 || receivePacket.getData()[i] == -1)
					break;
				
				builder.append((char)receivePacket.getData()[i]);
			}
			
			String message = builder.toString();
			System.out.println(message);
			
			String[] command = message.split("\\s+");
			
			if(command[0].equals("HELLO"))
			{
				System.out.println("HELLO packet received!");
				clientPort = receivePacket.getPort();
				clientAddress = receivePacket.getAddress();
			}
			else
			{
				wargame.messageHandler(command);
			}
		}
	}
	
	public void sendData(String message) {
		byte[] sendData = new byte[1024];
		sendData = (message + "\0").getBytes();
		
		if(clientAddress != null && isClient == false) {
		
			System.out.println("Sending " + message + " to " + clientAddress.toString() + " port " + Integer.toString(clientPort));
			DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, clientAddress, clientPort);
		
			try {
				serverSocket.send(sendPacket);
			} catch (IOException e) {
				// Failed to send packet.
				e.printStackTrace();
			}
		}
		else if(serverAddress != null && isClient == true) {
			System.out.println("Sending " + message + " to " + serverAddress.toString() + " port " + Integer.toString(port));
			DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, serverAddress, port);
			
			try {
				serverSocket.send(sendPacket);
			} catch (IOException e) {
				// Failed to send packet.
				e.printStackTrace();
			}
		}
	}
}
