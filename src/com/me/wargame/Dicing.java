package com.me.wargame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class Dicing implements Function{

	private Pin pin;
	
	private Texture dice1;
	private Texture dice2;
	private Texture dice3;
	private Texture dice4;
	private Texture dice5;
	private Texture dice6;
	
	private int frame = 0;
	private int temp = 0;

	private int x = GameContext.convertX(360);
	private int y = GameContext.convertY(220);
	private int width = GameContext.convertX(50);
	private int height = GameContext.convertY(50);
	
	public Dicing(Pin pin)
	{
		this.pin = pin;
		
		dice1 = new Texture(Gdx.files.internal("data/dice1.png"));
		dice2 = new Texture(Gdx.files.internal("data/dice2.png"));
		dice3 = new Texture(Gdx.files.internal("data/dice3.png"));
		dice4 = new Texture(Gdx.files.internal("data/dice4.png"));
		dice5 = new Texture(Gdx.files.internal("data/dice5.png"));
		dice6 = new Texture(Gdx.files.internal("data/dice6.png"));
	}

	int roll(){
		int random=(int)(Math.random()*6 + 1);

		int temp = random;
//		int temp = 8;
		
		switch(temp){
		case 1:
			GameContext.setDiceImage(dice1);
			break;
		case 2:
			GameContext.setDiceImage(dice2);
			break;
		case 3:
			GameContext.setDiceImage(dice3);
			break;
		case 4:
			GameContext.setDiceImage(dice4);
			break;
		case 5:
			GameContext.setDiceImage(dice5);
			break;
		case 6:
			GameContext.setDiceImage(dice6);
			break;
		}
		return temp;
	}
	void setTexture(int temp)
	{
		switch(temp){
		case 1:
			GameContext.setDiceImage(dice1);
			break;
		case 2:
			GameContext.setDiceImage(dice2);
			break;
		case 3:
			GameContext.setDiceImage(dice3);
			break;
		case 4:
			GameContext.setDiceImage(dice4);
			break;
		case 5:
			GameContext.setDiceImage(dice5);
			break;
		case 6:
			GameContext.setDiceImage(dice6);
			break;
		}
	}
	
	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void render(SpriteBatch batch) {
		// TODO Auto-generated method stub
		batch.draw(GameContext.getMapImage(), 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

		for(int i=0;i<GameContext.getBlockSize();++i)
			GameContext.getBlock()[i].Draw(batch);
		
		GameContext.getPinA().Draw(batch);
		GameContext.getPinB().Draw(batch);
		
		if(temp == 0){
			//System.out.println("Dicing");
						
			if(pin.getPredice() != 0)
			{
				System.out.println(pin.getPredice());
				GameContext.setDice(pin.getPredice());
				setTexture(GameContext.getDice());
				batch.draw(GameContext.getDiceImage(), x, y, width, height);
				
				pin.setPredice(0);
				temp = 1;
			}			
			else
			{
				GameContext.setDice(roll());
				setTexture(GameContext.getDice());
				batch.draw(GameContext.getDiceImage(), x, y, width, height);
			}
			
			if(Gdx.input.justTouched()){
				temp = 1;
			}
		}

		else if(temp == 1){
			batch.draw(GameContext.getDiceImage(), x, y, width, height);
			frame++;
			if(frame > 20 || Gdx.input.justTouched()){
				temp = 2;
			}
		}
		else if(temp == 2){
			WarGame.context.setFunction(new MainMap());
			frame = 0;
			temp = 0;
		}
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
	}
}