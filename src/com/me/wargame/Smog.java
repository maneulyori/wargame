package com.me.wargame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

public class Smog implements Function{
	
	private Pin pin;
	private Vector2 inputPos;
	private int random;
	private int temp = 0;
	private static int num = 0;	//Ȧ�� 1, ¦�� 2
	private static int AllPowerTurn = 0;	//�Ѱ���
	
	private Texture BackCheckImage;
	private Texture FirstImage;
	private Texture ShoutImage;
	private Texture RevengeImage;
	private Texture MistakeImage;
	private Texture GreedImage;
	private Texture FasterImage;
	private Texture AllPowerImage;
	
	private int x = GameContext.convertX(250);
	private int y = GameContext.convertY(50);
	private int width = GameContext.convertX(300);
	private int height = GameContext.convertY(400);
	
	public Smog(Pin pin)
	{		
		this.pin = pin;
		inputPos = new Vector2();
		
		BackCheckImage = new Texture(Gdx.files.internal("data/BackCheckCard.png"));
		FirstImage = new Texture(Gdx.files.internal("data/FirstCard.png"));
		ShoutImage = new Texture(Gdx.files.internal("data/ShoutCard.png"));
		RevengeImage = new Texture(Gdx.files.internal("data/RevengeCard.png"));
		MistakeImage = new Texture(Gdx.files.internal("data/MistakeCard.png"));
		GreedImage = new Texture(Gdx.files.internal("data/GreedCard.png"));
		FasterImage = new Texture(Gdx.files.internal("data/FasterCard.png"));
		AllPowerImage = new Texture(Gdx.files.internal("data/AllPowerCard.png"));
	
		random=(int)(Math.random()*8 + 1);
//		random = 6;
	}
	
	public static void setNum(int num)
	{
		Smog.num = num;
	}
	
	public static int getNum()
	{
		return Smog.num;
	}

	public static void setAllPowerTurn(int AllPowerTurn)
	{
		Smog.AllPowerTurn = AllPowerTurn;
	}
	
	public static int getAllPowerTurn()
	{
		return Smog.AllPowerTurn;
	}

	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void render(SpriteBatch batch) {
		batch.draw(GameContext.getMapImage(), 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		
		for(int i=0;i<GameContext.getBlockSize();++i)
			GameContext.getBlock()[i].Draw(batch);

		GameContext.getPinA().Draw(batch);
		GameContext.getPinB().Draw(batch);
		
		if(random == 1)
		{
			if(temp == 0){
				batch.draw(BackCheckImage, x, y, width, height);
				if(Gdx.input.justTouched()){
					temp = 1;
				}
			}
			else if(temp == 1){
				pin.changeDirection();
				pin.setBackCheck(1);
				
				temp = 0;
				WarGame.context.setFunction(new MainMap());
			}
		}
		else if(random == 2)
		{
			if(temp == 0){
				batch.draw(FirstImage, x, y, width, height);
				if(Gdx.input.justTouched()){
					temp = 1;
				}
			}
			else if(temp == 1){
				if(pin == GameContext.getPinA())
					pin.setPosition(0);
				else if(pin == GameContext.getPinB())
					pin.setPosition(9);
				
				temp = 0;
				WarGame.context.setFunction(new MainMap());
			}
		}
		else if(random == 3)
		{
			if(temp == 0){
				batch.draw(ShoutImage, x, y, width, height);
				if(Gdx.input.justTouched()){
					temp = 1;
				}
			}
			else if(temp == 1){
				if(Gdx.input.justTouched()){
					inputPos.x = Gdx.input.getX();
					inputPos.y = Gdx.graphics.getHeight() - Gdx.input.getY();

					if(pin == GameContext.getPinA())
					{
						for(int i=0;i<18;++i)
						{
							if(GameContext.getBlock()[i].getRectangle().contains(inputPos.x, inputPos.y))
							{
								if(GameContext.getBlock()[i].getTeam() == 2)
								{
									temp = 0;
									WarGame.context.setFunction(new SelectOddEven(pin, i));
								}
							}
						}
					}
					else if(pin == GameContext.getPinB())
					{
						for(int i=0;i<18;++i)
						{
							if(GameContext.getBlock()[i].getRectangle().contains(inputPos.x, inputPos.y))
							{
								if(GameContext.getBlock()[i].getTeam() == 1)
								{
									temp = 0;
									WarGame.context.setFunction(new SelectOddEven(pin, i));
								}
							}
						}
					}
				}
			}
		}
		else if(random == 4)
		{
			//�� �� ���� ������.
			//�ð������� ��Ÿ�� �ʿ䵵 ����
			if(temp == 0){
				batch.draw(RevengeImage, x, y, width, height);
				if(Gdx.input.justTouched()){
					temp = 1;
				}
			}
			else if(temp == 1){
				if(Gdx.input.justTouched()){
					inputPos.x = Gdx.input.getX();
					inputPos.y = Gdx.graphics.getHeight() - Gdx.input.getY();
					
					if(pin == GameContext.getPinA())
					{
						for(int i=0;i<18;++i)
						{
							if (GameContext.getBlock()[i].getRectangle().contains(inputPos.x, inputPos.y))
							{
								if(GameContext.getBlock()[i].getTeam() == 1)
								{
									GameContext.getBlock()[i].station(2, 0);
									
									if(i == 1 || i == 4)
										GameContext.getBlock()[i].setTexture(new Texture(Gdx.files.internal("data/block1B.png")));
									else if(i == 6 || i == 7)
										GameContext.getBlock()[i].setTexture(new Texture(Gdx.files.internal("data/block2B.png")));
									else if(i == 10 || i == 13)
										GameContext.getBlock()[i].setTexture(new Texture(Gdx.files.internal("data/block3B.png")));
									else if(i == 15 || i == 16)
										GameContext.getBlock()[i].setTexture(new Texture(Gdx.files.internal("data/block4B.png")));
																		
									temp = 0;
									WarGame.context.setFunction(new MainMap());
								}
							}		
						}
					}
					else if(pin == GameContext.getPinB())
					{
						for(int i=0;i<18;++i)
						{
							if (GameContext.getBlock()[i].getRectangle().contains(inputPos.x, inputPos.y))
							{
								if(GameContext.getBlock()[i].getTeam() == 2)
								{
									GameContext.getBlock()[i].station(1, GameContext.getBlock()[i].getSoldier());
									
									if(i == 1 || i == 4)
										GameContext.getBlock()[i].setTexture(new Texture(Gdx.files.internal("data/block1A.png")));
									else if(i == 6 || i == 7)
										GameContext.getBlock()[i].setTexture(new Texture(Gdx.files.internal("data/block2A.png")));
									else if(i == 10 || i == 13)
										GameContext.getBlock()[i].setTexture(new Texture(Gdx.files.internal("data/block3A.png")));
									else if(i == 15 || i == 16)
										GameContext.getBlock()[i].setTexture(new Texture(Gdx.files.internal("data/block4A.png")));

									temp = 0;
									WarGame.context.setFunction(new MainMap());
								}
							}		
						}
					}
				}
			}
		}
		else if(random == 5)
		{
			if(temp == 0){
				batch.draw(MistakeImage, x, y, width, height);
				if(Gdx.input.justTouched()){
					temp = 1;
				}
			}
			else if(temp == 1){
				pin.loseSoldier(100);
				pin.losetotalSoldier(100);
				
				temp = 0;
				WarGame.context.setFunction(new MainMap());
			}
		}
		else if(random == 6)
		{
			if(temp == 0){
				batch.draw(GreedImage, x, y, width, height);
				if(Gdx.input.justTouched()){
					temp = 1;
				}
			}
			else if(temp == 1){
				pin.setGreed(3);
				
				temp = 0;
				WarGame.context.setFunction(new MainMap());
			}
		}
		else if(random == 7)
		{
			System.out.println("FasterImage");
			
			if(temp == 0){
				batch.draw(FasterImage, x, y, width, height);
				if(Gdx.input.justTouched()){
					temp = 1;
				}
			}
			else if(temp == 1){
				
				if(Gdx.input.justTouched()){
					inputPos.x = Gdx.input.getX();
					inputPos.y = Gdx.graphics.getHeight() - Gdx.input.getY();

					for(int i=0;i<18;++i)
					{
						if (GameContext.getBlock()[i].getRectangle().contains(inputPos.x, inputPos.y))
						{
							if(pin == GameContext.getPinA())
							{
								if(i != GameContext.getPinB().getPosition())
								{
									pin.setPosition(i);
									temp = 0;
									WarGame.context.setFunction(new MainMap());
								}
							}
							else if(pin == GameContext.getPinB())
							{
								if(i != GameContext.getPinA().getPosition())
								{
									pin.setPosition(i);
									temp = 0;
									WarGame.context.setFunction(new MainMap());
								}
							}
						}
					}
				}
			}
		}
		else if(random == 8)
		{
			if(temp == 0){
				batch.draw(AllPowerImage, x, y, width, height);
				if(Gdx.input.justTouched()){
					temp = 1;
				}
			}
			else if(temp == 1){
				
				Smog.setAllPowerTurn(5);
				
				temp = 0;
				WarGame.context.setFunction(new MainMap());
			}
		}
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
	}
}